<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\RegisaterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['guestforum'])->group(function () {
  Route::get('/login', [LoginController::class, 'index'])->name('login.index');
  Route::post('/login', [LoginController::class, 'login'])->name('login.prosses');
  Route::resource('register', RegisaterController::class);
});

Route::middleware(['authforum'])->group(function () {
  Route::get('/', [HomeController::class, 'index'])->name('home');
  Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
  Route::resource('kategori', KategoriController::class);

  Route::get('question', [PertanyaanController::class, 'index'])->name('question.index');
  Route::post('question', [PertanyaanController::class, 'store'])->name('question.store');
});

// CRUD Kategori
Route::resource('kategori', KategoriController::class);


// // Route untuk membuat pertanyaan
// Route::get('/question/create', [QuestionsController::class, 'create']);

// // Route untuk menyimpan inputan kedalam database tabel pertanyaan
// Route::post('/question', [QuestionsController::class, 'store']);

// // Read Data
// // Route mengarah ke halaman tampil semua data di tabel pertanyaan
// Route::get('/question', [QuestionsController::class, 'index']);

// // Route read more
// Route::get('/uestion/{id}', [QuestionsController::class, 'show']);
