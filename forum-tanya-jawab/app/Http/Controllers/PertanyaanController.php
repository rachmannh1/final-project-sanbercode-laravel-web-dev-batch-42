<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PertanyaanController extends Controller
{
    public function index()
    {
        $daftarKategori = Kategori::all();

        $daftarPertanyaan = Pertanyaan::with('user', 'kategori')
            ->get();

        return view('questions.index', [
            'daftarKategori' => $daftarKategori,
            'daftarPertanyaan' => $daftarPertanyaan
        ]);
    }

    public function store(Request $request)
    {
        $userLogin = Auth::guard('auth')->user();
        $pertanyaan = new Pertanyaan;
        $pertanyaan->tulisan = $request->tulisan;
        $pertanyaan->id_user = $userLogin->id;
        $pertanyaan->id_kategori = $request->kategori;
        $pertanyaan->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);

        $pertanyaan->delete();

        return redirect('/question');
    }
}
