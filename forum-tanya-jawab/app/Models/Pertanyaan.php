<?php

namespace App\Models;

use App\Models\Kategori;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $fillable = ['tulisan', 'gambar', 'id_kategori', 'id_user'];
    use HasFactory;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'id_user');
    }

    public function kategori ()
    {
        return $this->hasOne(Kategori::class, 'id', 'id_kategori');
    }
}
