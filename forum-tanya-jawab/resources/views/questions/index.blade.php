@extends('layouts.dashboard')

@section('title')
    List questions
@endsection

@section('content')
<div class="row mb-5">
  <div class="col-lg-12">
    <button class="btn btn-sm btn-primary" id="btn-show-modal-create">Add question</button>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
              <tr>
                  <th>#</th>
                  <th>Content</th>
                  <th>Created by</th>
                  <th>categories</th>
                  <th>Actions</th>
              </tr>
          </thead>
          <tbody>
            @foreach ($daftarPertanyaan as $item)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$item->tulisan}}</td>
                  <td>{{$item->user->name}}</td>
                  <td>{{$item->kategori->name}}</td>
                  <td>
                    <form action="/question/{{{$item->id}}}" method="POST" class="d-flex flex-lg-row flex-column justify-content-center align-items-center g-4 gap-3">
                      @csrf
                      @method('delete')
                      <a href="/question/{{$item->id}}" class="btn btn-info btn-sm mx-3 lg-my-0 my-3">Detail</a>
                      <a href="/question/{{$item->id}}/edit" class="btn btn-warning btn-sm mx-3 lg-my-0 my-3">Edit</a>
  
                      <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  </form>
                  </td>
                </tr>
            @endforeach
          </tbody>
      </table>
    </div>
  </div>
</div>

{{-- Modal --}}
<div class="modal" tabindex="-1" id="modal_create_question">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create Question</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('question.store')}}" method="post">
        @csrf
      <div class="modal-body">
        <div class="row mb-2">
          <div class="col-lg-12">
            <label class="form-label">Content</label>
            <textarea name="tulisan" class="form-control" placeholder="Lorem ipsum sit dolor ..."></textarea>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <label>Category</label>
            <select name="kategori" class="form-control w-100">
              @foreach ($daftarKategori as $item)
                  <option value="{{$item->id}}">{{$item->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
   
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
    <script>
      $('#dataTable').DataTable({
        
      });

      $('#btn-show-modal-create').click(function (e) {
        e.preventDefault();

        $('#modal_create_question').modal('show');
      });
    </script>
@endpush