@extends('layouts.dashboard')

@section('title')
    Halaman Tambah Pertanyaan
@endsection

@section('content')
<form action="/question" method="post" enctype="multipart/form-data">
  @csrf 
    <div class="mb-3">
        <label for="tulisan" class="form-label">Tulisan :</label>
        <textarea class="form-control" name="tulisan" id="tulisan" rows="3"></textarea>
      </div>

      @error('tulisan')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror

      <div class="mb-3">
        <label for="formFile" class="form-label">Gambar :</label>
        <input class="form-control" name="gambar" type="file" id="formFile">
      </div>

      @error('gambar')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror

      <div class="mb-3">
        <label for="kategori" class="form-label">Kategori :</label>
        <input type="text" class="form-control" name="kategori" id="kategori" aria-describedby="kategori">
      </div>

      @error('kategori')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror

    <button type="submit" class="btn btn-primary w-100 mb-2">Submit</button>
    <a href="/question" class="btn btn-danger w-100">Cancel</a>
  </form>
  
@endsection