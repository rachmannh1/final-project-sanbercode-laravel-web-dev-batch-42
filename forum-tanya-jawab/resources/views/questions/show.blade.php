@extends('layouts.dashboard')

@section('title')
    Halaman Pertanyaan
@endsection

@section('content')
<a href="/question/create" class="btn btn-primary btn-sm my-3">Buat Pertanyaan</a>

<div class="row row-cols-2 row-cols-md-4 g-4">
    @forelse ($pertanyaan as $data)
    <div class="col">
        <div class="card">
          <img src="{{asset('/img/' . $data->gambar)}}" height="200" class="card-img-top" alt="Gambar Pertanyaan">
          <div class="card-body">
            <h5 class="card-title">{{Str::limit($data->tulisan, 30)}}</h5>
            <span class="badge text-bg-secondary mb-3">{{$data->kategori}}</span>
            <br>
            <a href="/question/{{$data->id}}" class="btn btn-info btn-block">Read More</a>

            <div class="row mt-3 text-center">
                <div class="col-6">
                    <a href="/question/{{$data->id}}" class="btn btn-warning btn-block">Edit</a>
                </div>
                <div class="col-6">
                    <a href="/question/{{$data->id}}" class="btn btn-danger">Delete</a>
                </div>
            </div>
          </div>
        </div>
      </div>
    @empty
        <h1>Tidak Ada Pertanyaan</h1>
    @endforelse
@endsection