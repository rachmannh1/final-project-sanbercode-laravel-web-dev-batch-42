@extends('layouts.dashboard')

@section('title')
    Form Pertanyaan
@endsection

@section('content')
<form>
    <div class="mb-3">
        <label for="tulisan" class="form-label">Tulisan :</label>
        <textarea class="form-control" name="tulisan" id="tulisan" rows="3"></textarea>
      </div>

      <div class="mb-3">
        <label for="formFile" class="form-label">Gambar :</label>
        <input class="form-control" name="gambar" type="file" id="formFile">
      </div>

      <div class="mb-3">
        <div class="form-group">
            <label for="kategori">Kategori</label>
            <select class="form-control" name="kategori" id="kategori">
              <option>-- PILIH KATEGORI --</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
      </div>

    <button type="submit" class="btn btn-primary w-100">Submit</button>
  </form>
  
@endsection